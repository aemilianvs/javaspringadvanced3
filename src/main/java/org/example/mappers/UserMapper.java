package org.example.mappers;

import org.example.domain.UserModel;
import org.example.dto.UserDto;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring")
public interface UserMapper {

    UserModel userDtoToUserModel(UserDto userDto);

    @Mapping(source = "password", target = "password", ignore = true)
    UserDto userModelToUserDto(UserModel userModel);
}
