package org.example.controllers;

import org.example.domain.UserModel;
import org.example.dto.UserDto;
import org.example.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/users")
public class UsersController {

    @Autowired
    private UserService userService;

    @GetMapping("/byUsername")
    public Page<UserDto> getUsersByUsername(
            @RequestParam String username,
            @RequestParam int currentPage,
            @RequestParam int itemsPerPage){
        return userService.findAllMatching(username, PageRequest.of(currentPage, itemsPerPage, Sort.by("firstName", "id")));
    }

    @GetMapping("/byFirstName")
    public Page<UserDto> getUsersByFirstName(
            @RequestParam String firstName,
            @RequestParam int currentPage,
            @RequestParam int itemsPerPage){
        return userService.findByUsername(firstName, PageRequest.of(currentPage, itemsPerPage, Sort.by("username")));
    }

    @GetMapping("/getOne")
    public UserDto getOneUser(@RequestParam String username) {
        return userService.findOne(username);
    }

    @PostMapping("/save")
    public UserDto saveUser(@RequestBody UserDto userDto) {
        return userService.saveUser(userDto);
    }

    @GetMapping("/search")
    public List<UserDto> getUsers(@RequestParam String username1, @RequestParam String username2, @RequestParam String email) {
        return userService.findByNames(username1, username2, email);
    }

    @PostMapping
    public UserDto addUser(@RequestBody UserDto user) { // ref 1
        return userService.add(user); // user = ref 1
        // returning ref 2
    }
}
