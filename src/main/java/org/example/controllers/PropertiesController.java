package org.example.controllers;

import com.fasterxml.jackson.databind.annotation.JsonAppend;
import org.example.config.PropertiesCategory;
import org.example.config.PropertiesInfo;
import org.example.config.PropertiesPerson;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/config")
public class PropertiesController {

    @Autowired
    private PropertiesCategory propertiesCategory;

    @Autowired
    private PropertiesPerson propertiesPerson;

    @Autowired
    private PropertiesInfo propertiesInfo;

    @GetMapping("/category")
    public PropertiesCategory category() {
        propertiesCategory.getItems().add("Peach");
        return propertiesCategory;
    }

    @GetMapping("/person")
    public PropertiesPerson person() {
        System.out.println(propertiesPerson.getVisitor().getAge());
        return propertiesPerson;
    }

    @GetMapping("/info")
    public PropertiesInfo info() {
        return propertiesInfo;
    }
}
