package org.example.aspects;

import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;

@Component
@Aspect
public class UserServiceAspect {

    @Pointcut("execution(public org.example.domain.UserModel org.example.repositories.UserRepository.add(org.example.domain.UserModel))")
    public void userAddExecution() { }

    @Pointcut("within(org.example.controllers.*)")
    public void withinControllers() { }

    @Pointcut("@within(org.springframework.stereotype.Repository)")
    public void withinRepositoryAnnotation() { }

    @Pointcut("execution(* *..findBy*(String,..))")
    public void executionPointcut() { }

    @After("userAddExecution()")
    public void afterUserAddedExecution() {
        System.out.println("After userAddExecution()");
    }

    @After("withinControllers() || userAddExecution()")
    public void afterWithinControllers() {
        System.out.println("After withinControllers()");
    }

    @After("executionPointcut()")
    public void afterExecutionPointcut() {
        System.out.println("After executionPointcut()");
    }

    @After("withinRepositoryAnnotation()")
    public void afterRepositoryAnnotation() {
        System.out.println("After withinRepositoryAnnotation()");
    }
}
