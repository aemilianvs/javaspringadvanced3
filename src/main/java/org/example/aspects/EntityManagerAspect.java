package org.example.aspects;

import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.*;
import org.springframework.stereotype.Component;

@Aspect
@Slf4j
@Component
public class EntityManagerAspect {

    private int operationsCount = 0;

//    @Pointcut("execution(public org.example.domain.UserModel org.example.repositories.UserRepository.add(org.example.domain.UserModel))")
    @Pointcut("target(javax.persistence.EntityManager)")
    private void onEntityManager() { }

    @After("onEntityManager()")
    public void afterOnEntityManager() {
        System.out.println("In the aspect, after onEntityManager, operationsCount is " + (++operationsCount));
    }

    @Before("onEntityManager()")
    public void beforeOnEntityManager() {
        System.out.println("In the aspect, before onEntityManager");
    }

    @Around("onEntityManager()")
    public Object aroundEntityManager(ProceedingJoinPoint joinPoint) throws Throwable {
        System.out.println("In the aspect, before proceeding");

        Object obj = joinPoint.proceed();

        System.out.println("In the aspect, after proceeding");

        return obj;
    }
}
