package org.example.listeners;

import org.example.events.AfterAddingUserEvent;
import org.example.events.BeforeAddingUserEvent;
import org.springframework.context.event.EventListener;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

@Async
@Component
public class UserServiceListeners {

    @EventListener
    public void handleBeforeUserAddedEvent(BeforeAddingUserEvent event) {
        System.out.println("In the handler: before user added timestamp: " + event.getTimestamp());
    }

    @EventListener
    public void handleAfterUserAddedEvent(AfterAddingUserEvent event) {
        System.out.println("In the handler: after user added timestamp: " + event.getTimestamp());
    }
}
