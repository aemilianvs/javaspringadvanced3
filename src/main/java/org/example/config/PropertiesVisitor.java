package org.example.config;

import lombok.AllArgsConstructor;
import lombok.Data;

import javax.validation.constraints.Min;
import javax.validation.constraints.Size;

@Data
@AllArgsConstructor
public class PropertiesVisitor {

    @Size(min = 3, max = 20)
    private String name;
    private int age;
    private int hourArrival;
}
