package org.example.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

@Data
@ConfigurationProperties(prefix = "info")
public class PropertiesInfo {
    private PropertiesCategory category;
    private PropertiesPerson person;
}
