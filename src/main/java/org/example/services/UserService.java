package org.example.services;

import lombok.extern.slf4j.Slf4j;
import org.example.domain.UserModel;
import org.example.dto.UserDto;
import org.example.events.AfterAddingUserEvent;
import org.example.events.BeforeAddingUserEvent;
import org.example.mappers.UserMapper;
import org.example.repositories.PagedUserRepository;
import org.example.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.data.domain.*;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

@Slf4j
@Service
public class UserService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private PagedUserRepository pagedUserRepository;

    @Autowired
    private UserMapper userMapper;

    @Autowired
    private ApplicationEventPublisher publisher;

    public UserDto saveUser(UserDto userDto) {
        UserModel model = pagedUserRepository.save(userMapper.userDtoToUserModel(userDto));
        return userMapper.userModelToUserDto(model);
    }

    public Page<UserDto> findByUsername(String username, Pageable pageable) {
        Page<UserModel> models = pagedUserRepository.findByUsernameContains(username, pageable);
        List<UserDto> userDtos = models.getContent()
                .stream()
                .map(userMapper::userModelToUserDto)
                .collect(Collectors.toList());
        return new PageImpl<>(userDtos, models.getPageable(), models.getTotalElements());
    }

    public UserDto findOne(String username) {
        UserModel model = new UserModel();
        model.setUsername(username);
        UserModel result = pagedUserRepository.findOne(Example.of(model)).orElse(null);
        return userMapper.userModelToUserDto(result);
    }

    public Page<UserDto> findAllMatching(String username, Pageable pageable) {
        UserModel model = new UserModel();
        model.setUsername(username);
        ExampleMatcher matcher = ExampleMatcher.matching().withStringMatcher(ExampleMatcher.StringMatcher.CONTAINING);
        Page<UserModel> models = pagedUserRepository.findAll(Example.of(model, matcher), pageable);
        List<UserDto> userDtos = models.getContent()
                .stream()
                .map(userMapper::userModelToUserDto)
                .collect(Collectors.toList());
        return new PageImpl<>(userDtos, models.getPageable(), models.getTotalElements());
    }

    public Page<UserDto> findByFirstName(String firstName, Pageable pageable) {
        Page<UserModel> models = pagedUserRepository.findByFirstNameContains(firstName, pageable);
        List<UserDto> userDtos = models.getContent()
                .stream()
                .map(userMapper::userModelToUserDto)
                .collect(Collectors.toList());
        return new PageImpl<>(userDtos, models.getPageable(), models.getTotalElements());
    }

    public List<UserDto> findByName(String username) {
        List<UserModel> userModels = userRepository.findByNames(username, "", "");
        return userModels
                .stream()
                .map(userMapper::userModelToUserDto)
                .collect(Collectors.toList());
    }

    public List<UserDto> findByNames(String username1, String username2, String email) {

//        List<UserDto> userDtos = new ArrayList<>();
//        List<UserModel> userModels = userRepository.findByNames(username1, username2, email);
//        for (UserModel userModel :
//                userModels) {
//            UserDto userDto = userMapper.userModelToUserDto(userModel);
//            userDtos.add(userDto);
//        }
//        return userDtos;

        List<UserModel> userModels = userRepository.findByNames(username1, username2, email);
//
//        List<String> emails = userModels.stream()
//                .map(userModel -> userModel.getEmail())
//                .collect(Collectors.toList());
//
        return userModels
            .stream()
//            .map(userModel -> userMapper.userModelToUserDto(userModel))
            .map(userMapper::userModelToUserDto)
            .collect(Collectors.toList());
    }

    public UserDto add(UserDto userFromController) { // ref 1
        // receives DTO - Data Transfer Object

//        System.out.println("Before publishing the first event");
//        log.info("Before adding user");
//        this.publisher.publishEvent(new BeforeAddingUserEvent(userFromController, LocalDateTime.now()));
//        System.out.println("After publishing the first event");

        UserModel userModel = userMapper.userDtoToUserModel(userFromController);
        // translate DTO to domain model
        userRepository.add(userModel); // user = ref 1
        // we need to translate user model to user dto
        UserDto returnedUserDto = userMapper.userModelToUserDto(userModel);

//        System.out.println("Before publishing the second event");
//        this.publisher.publishEvent(new AfterAddingUserEvent(returnedUserDto, LocalDateTime.now()));
//        System.out.println("After publishing the second event");

        // returns the DTO
        return returnedUserDto; // ref 1
    }
}
