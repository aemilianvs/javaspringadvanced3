package org.example.repositories;

import org.example.domain.UserModel;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;

@Repository
@Transactional
public class UserRepository {

    private final EntityManager entityManager;

    private int operationsCount = 0;

    public UserRepository(final EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    public List<UserModel> findByNames(String username1, String username2, String email) {
        // SELECT FROM users WHERE username = 'mine' OR username = 'yours' AND email LIKE '%hers%';

        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<UserModel> criteriaQuery = criteriaBuilder.createQuery(UserModel.class);
        Root<UserModel> root = criteriaQuery.from(UserModel.class);

        List<Predicate> predicates = new ArrayList<>();
        predicates.add(criteriaBuilder.equal(root.get("username"), username1));
        predicates.add(criteriaBuilder.equal(root.get("username"), username2));
        predicates.add(criteriaBuilder.like(root.get("email"), email));

        Predicate whereClause = criteriaBuilder.or(predicates.toArray(new Predicate[0]));

        CriteriaQuery<UserModel> finalQuery = criteriaQuery.select(root).where(whereClause);

        return entityManager.createQuery(finalQuery).getResultList();
    }

    public UserModel add(UserModel user) { // ref 1
//        user = new UserModel(); // ref 2
//        entityManager.persist(user); // ref 2
//        return user; // ref 2

        System.out.println("Before calling persist, in the repository");
        entityManager.persist(user); // ref 1
        System.out.println("After calling persist, in the repository");
        return user; // ref 1
    }
}
