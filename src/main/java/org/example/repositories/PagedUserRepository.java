package org.example.repositories;

import org.example.domain.UserModel;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository
public interface PagedUserRepository extends JpaRepository<UserModel, UUID> {
    Page<UserModel> findByUsernameContains(String username, Pageable pageable);
    Page<UserModel> findByFirstNameContains(String firstName, Pageable pageable);
}
